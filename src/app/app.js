import angular from 'angular';
import AppCtrl from './app.controller';
import HomeCtrl from './home/home.controller';
import uiRouter from 'angular-ui-router';

const MODULE_NAME = 'app';


function routing($urlRouterProvider, $stateProvider) {
    $urlRouterProvider
        .otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            template : require('./home/home.html'),
            controller: 'HomeCtrl',
            controllerAs: 'vm'
        })
        .state('page', {
            url: '/qwe',
            template: '',
            controller: 'AppCtrl'
        });
}

angular.module(MODULE_NAME, [uiRouter])
    .controller('AppCtrl', AppCtrl)
    .controller('HomeCtrl', HomeCtrl)
    .config(routing)
    .value('lodash', lodash)
    .config(function ($locationProvider, $urlRouterProvider) {
        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
    });