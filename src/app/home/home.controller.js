export default class HomeCtrl {
    constructor(lodash, $state) {
        this.$state = $state;
    }

    goHome() {
        this.$state.go('page', {});
    }
}